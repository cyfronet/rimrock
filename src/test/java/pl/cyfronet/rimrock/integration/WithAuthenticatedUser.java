package pl.cyfronet.rimrock.integration;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.cyfronet.rimrock.ProxyFactory;

import static org.mockito.Mockito.when;

public abstract class WithAuthenticatedUser {
    @Autowired
    ProxyFactory proxyFactory;

    @Mock private Authentication auth;

    @BeforeEach
    public void setUp() throws Exception {
        when(auth.getName()).thenReturn(proxyFactory.getProxyUserLogin());
        when(auth.getCredentials()).thenReturn(proxyFactory.getProxy());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
