package pl.cyfronet.rimrock.integration;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import pl.cyfronet.rimrock.services.RunnersFactory;
import pl.cyfronet.rimrock.runners.RunResults;

@SpringBootTest
public class ConcurrentSshSessionsTest extends WithAuthenticatedUser {
	private static final String HOST = "pro.cyfronet.pl";

	@Autowired RunnersFactory runnersFactory;

	@Test
	public void testMultipleSessions() throws InterruptedException {
		List<Thread> threads = new ArrayList<>();

		for(int i = 0; i < 20; i++) {
			int index = i;

			Thread t = new Thread(() -> {
				try {
					RunResults result = runnersFactory.build().run(HOST, "sleep 2; echo hello " + index , null, 20000);
					assertTrue(result.getOutput().startsWith("hello"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			threads.add(t);
			t.start();
			sleep(100);
		}

		for(Thread t : threads) {
			t.join();
		}
	}
}
