package pl.cyfronet.rimrock.integration.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import groovyjarjarpicocli.CommandLine.Parameters;
import pl.cyfronet.rimrock.ProxyFactory;
import pl.cyfronet.rimrock.controllers.jobs.JobActionRequest;
import pl.cyfronet.rimrock.controllers.jobs.JobInfo;
import pl.cyfronet.rimrock.controllers.jobs.SubmitRequest;
import pl.cyfronet.rimrock.domain.Job;
import pl.cyfronet.rimrock.gsi.ProxyHelper;
import pl.cyfronet.rimrock.repositories.JobRepository;

@SpringBootTest()
public class JobsControllerMvcTest {
	private static final Logger log = LoggerFactory.getLogger(JobsControllerMvcTest.class);
	
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private ProxyFactory proxyFactory;
	
	@Autowired
	private ProxyHelper proxyHelper;
	
	@Autowired
	private JobRepository jobRepository;
	
	private MockMvc mockMvc;

	@Parameters
    public static Stream<Arguments> data() {
    	return Fixtures.jobParameters();
    }
    
	@BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
    }
	
	@ParameterizedTest
	@MethodSource("data")
	public void testSimpleJobSubmission(String host, String script) throws JsonProcessingException, Exception {
		SubmitRequest submitRequest = new SubmitRequest();
		submitRequest.setHost(host);
		submitRequest.setScript(script);
		
		MvcResult result = mockMvc.perform(post("/api/jobs")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(submitRequest)))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andReturn();
		
		String body = result.getResponse().getContentAsString();
		JobInfo submitResult = mapper.readValue(body, JobInfo.class);
		String jobId = submitResult.getJobId();
		log.info("Checking job status for job id {}", jobId);
		
		mockMvc.perform(get("/api/jobs/" + jobId)
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void testStatusRetrievalForInvalidJobId() throws Exception {
		mockMvc.perform(get("/api/jobs/nonexisting_id.pro.cyfronet.pl")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.status", is("ERROR")))
				.andExpect(status().isNotFound());
	}
	
	@ParameterizedTest
	@MethodSource("data")
	public void testRetrievalNotOwnedJob(String host, String script) throws Exception {
		Job job = new Job("not_owned_job", "FINISHED", "", "", "other_user_login", host, null);
		jobRepository.save(job);		
		
		mockMvc.perform(get("/api/jobs/not_owned_job")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))
				.andExpect(status().isNotFound());
	}
	
	@ParameterizedTest
	@MethodSource("data")
	public void testGlobalStatusRetrieval() throws Exception {
		mockMvc.perform(get("/api/jobs")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@ParameterizedTest
	@MethodSource("data")
	public void testJobDeleting(String host, String script) throws JsonProcessingException, Exception {
		SubmitRequest submitRequest = new SubmitRequest();
		submitRequest.setHost(host);
		submitRequest.setScript(script);
		
		MvcResult result = mockMvc.perform(post("/api/jobs")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(submitRequest)))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andReturn();
		
		String body = result.getResponse().getContentAsString();
		JobInfo submitResult = mapper.readValue(body, JobInfo.class);
		String jobId = submitResult.getJobId();
		log.info("Stopping job for job id {}", jobId);
		
		mockMvc.perform(delete("/api/jobs/" + jobId)
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))
				
				.andDo(print())
				
				.andExpect(status().isNoContent());
	}
	
	@ParameterizedTest
	@MethodSource("data")
	public void testJobAborting(String host, String script) throws JsonProcessingException, Exception {
		SubmitRequest submitRequest = new SubmitRequest();
		submitRequest.setHost(host);
		submitRequest.setScript(script);
		
		MvcResult result = mockMvc.perform(post("/api/jobs")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(submitRequest)))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andReturn();
		
		String body = result.getResponse().getContentAsString();
		JobInfo submitResult = mapper.readValue(body, JobInfo.class);
		String jobId = submitResult.getJobId();
		log.info("Aborting job for job id {}", jobId);
		
		JobActionRequest actionRequest = new JobActionRequest();
		actionRequest.setAction("abort");
		mockMvc.perform(put("/api/jobs/" + jobId)
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(actionRequest)))
				
				.andDo(print())
				
				.andExpect(status().isNoContent());
		mockMvc.perform(get("/api/jobs/" + jobId)
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))
				
				.andDo(print())
				
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.status", is("ABORTED")))
				.andExpect(status().isOk());
	}
}