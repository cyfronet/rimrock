package pl.cyfronet.rimrock.integration.rest;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;

import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.MockMvcConfigurer;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.cyfronet.rimrock.ProxyFactory;
import pl.cyfronet.rimrock.controllers.run.RunRequest;
import pl.cyfronet.rimrock.gsi.ProxyHelper;

/**
 * DH: Consider using https://code.google.com/p/rest-assured/ for RESt tests
 *
 * @author daniel
 *
 */
@SpringBootTest
public class RunControllerMvcTest {
	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private ProxyFactory proxyFactory;

	@Autowired
	private ProxyHelper proxyHelper;

	@Value("${run.timeout.millis}")
	private int runTimeoutMillis;

	private MockMvc mockMvc;

	private static Stream<Arguments> data() {
		return Stream.of(
				Arguments.of("pro.cyfronet.pl")
			);
	}

	@BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
    }

	@ParameterizedTest
	@MethodSource("data")
	public void testSimpleRun(String host) throws Exception {
		RunRequest runRequest = new RunRequest();
		runRequest.setCommand("pwd");
		runRequest.setHost(host);

		mockMvc.perform(post("/api/process")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(runRequest)))

				.andDo(print())

				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.status", is("OK")))
				.andExpect(jsonPath("$.exit_code", is(0)))
				.andExpect(jsonPath("$.standard_output", anyOf(startsWith("/people"),
						startsWith("/net/people"), startsWith("/mnt/auto/people"))));
	}

	@ParameterizedTest
	@MethodSource("data")
	public void testExitCodeAndStandardErrorPresent(String host) throws Exception {
		RunRequest runRequest = new RunRequest();
		//at least the second mkdir command will return a 1 exit code
		runRequest.setCommand("echo 'error' > /dev/stderr; mkdir /tmp/test; mkdir /tmp/test");
		runRequest.setHost(host);

		mockMvc.perform(post("/api/process")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(runRequest)))

				.andDo(print())

				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.exit_code", is(1)))
				.andExpect(jsonPath("$.status", is("OK")))
				.andExpect(jsonPath("$.error_output", startsWith("error")));
	}

	@Test
	public void testNotNullValidation() throws JsonProcessingException, Exception {
		RunRequest runRequest = new RunRequest();
		mockMvc.perform(post("/api/process")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(runRequest)))

				.andDo(print())

				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("$.exit_code", is(-1)))
				.andExpect(jsonPath("$.standard_output", is(equalTo(null))))
				.andExpect(jsonPath("$.status", is("ERROR")))
				.andExpect(jsonPath("$.error_message", containsString("host:")));
	}

	@ParameterizedTest
	@MethodSource("data")
	public void testMultilineOutput(String host) throws Exception {
		RunRequest runRequest = new RunRequest();
		runRequest.setCommand("echo hello1; echo hello2; echo hello3");
		runRequest.setHost(host);

		mockMvc.perform(post("/api/process")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(runRequest)))

				.andDo(print())

				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is("OK")))
				.andExpect(jsonPath("$.exit_code", is(0)))
				.andExpect(jsonPath("$.standard_output", is("hello1\nhello2\nhello3")));
	}

	@ParameterizedTest
	@MethodSource("data")
	public void testTimeout(String host) throws Exception {
		RunRequest runRequest = new RunRequest();
		runRequest.setCommand("echo 'going to sleep'; sleep " + ((runTimeoutMillis / 1000) + 5));
		runRequest.setHost(host);

		mockMvc.perform(post("/api/process")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(runRequest)))

				.andDo(print())

				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isRequestTimeout())
				.andExpect(jsonPath("$.status", is("ERROR")))
				.andExpect(jsonPath("$.exit_code", is(-1)))
				//TODO: reading output can be improved
				//.andExpect(jsonPath("$.standard_output", is("going to sleep")))
				.andExpect(jsonPath("$.error_message", startsWith("timeout")));
	}

	@ParameterizedTest
	@MethodSource("data")
	public void testWorkingDirectoryOverride(String host) throws JsonProcessingException, Exception {
		RunRequest runRequest = new RunRequest();
		runRequest.setCommand("pwd");
		runRequest.setHost(host);
		runRequest.setWorkingDirectory("/tmp");

		mockMvc.perform(post("/api/process")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(runRequest)))

				.andDo(print())

				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is("OK")))
				.andExpect(jsonPath("$.exit_code", is(0)))
				.andExpect(jsonPath("$.standard_output", is("/tmp")));
	}
}