package pl.cyfronet.rimrock.integration.rest;

import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

public class Fixtures {
		private static final String grant = "plgprimage4";

	public static Stream<Arguments> jobParameters() {
		return Stream.of(
				Arguments.of(
					"prometheus.cyfronet.pl",
					"#!/bin/bash\n#SBATCH -A " + grant + "\necho hello\nexit 0"
				)
			 );
	}

	public static Stream<Arguments> jobWithDirOverrideParameters() {
		String randomDirectory = UUID.randomUUID().toString();
		return Stream.of(
				Arguments.of(
					"prometheus.cyfronet.pl",
					"#!/bin/bash\n#SBATCH -A " + grant + "\necho hello\nexit 0",
					"/net/people/{userLogin}/" + randomDirectory
				)
			);
	}
}
