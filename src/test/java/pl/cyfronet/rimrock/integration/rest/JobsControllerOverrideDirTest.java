package pl.cyfronet.rimrock.integration.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.cyfronet.rimrock.ProxyFactory;
import pl.cyfronet.rimrock.controllers.jobs.JobInfo;
import pl.cyfronet.rimrock.controllers.jobs.SubmitRequest;
import pl.cyfronet.rimrock.gsi.ProxyHelper;

@SpringBootTest
public class JobsControllerOverrideDirTest {
	private static final Logger log = LoggerFactory.getLogger(JobsControllerOverrideDirTest.class);

	@Value("${test.user.login}")
    private String userLogin;

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private ProxyFactory proxyFactory;

	@Autowired
	private ProxyHelper proxyHelper;

	private MockMvc mockMvc;

    public static Stream<Arguments> data() {
    	return Fixtures.jobWithDirOverrideParameters();
    }

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
    }

	@ParameterizedTest
	@MethodSource("data")
	public void testOverrideWorkingDirectory(String host, String script, String workingDirectory) throws JsonProcessingException, Exception {
		SubmitRequest submitRequest = new SubmitRequest();
		submitRequest.setHost(host);
		submitRequest.setScript(script);
		submitRequest.setWorkingDirectory(getWorkingDirectory(workingDirectory));

		MvcResult result = mockMvc.perform(post("/api/jobs")
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(submitRequest)))

				.andDo(print())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andReturn();

		String body = result.getResponse().getContentAsString();
		JobInfo submitResult = mapper.readValue(body, JobInfo.class);
		String jobId = submitResult.getJobId();
		log.info("Checking job status for job id {}", jobId);

		mockMvc.perform(get("/api/jobs/" + jobId)
				.header("PROXY", proxyHelper.encodeProxy(proxyFactory.getProxy())))

				.andDo(print())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	private String getWorkingDirectory(String workingDirectory) {
	   return workingDirectory.replaceAll("\\{userLogin\\}", userLogin);
    }
}