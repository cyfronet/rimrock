package pl.cyfronet.rimrock.integration;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import pl.cyfronet.rimrock.ProxyFactory;
import pl.cyfronet.rimrock.services.RunnersFactory;
import pl.cyfronet.rimrock.runners.RunResults;

@SpringBootTest
public class CleanHistoryAndLongOutputTest extends WithAuthenticatedUser {
	private static final Logger log = LoggerFactory.getLogger(CleanHistoryAndLongOutputTest.class);

	@Autowired private ProxyFactory proxyFactory;
	@Autowired private RunnersFactory runnersFactory;

	@Value("${run.timeout.millis}") private int runTimeoutMillis;

	@Test
	public void testWhetherHistoryIsClean() throws CredentialException, KeyStoreException,
			CertificateException, GSSException, IOException,
			InterruptedException, Exception {
		RunResults history = runnersFactory.build().run("pro.cyfronet.pl",
				"cat .bash_history | sha1sum", null, runTimeoutMillis);
		log.info("History: {}", history.getOutput());
		runnersFactory.build().run("pro.cyfronet.pl", "echo hello", null, runTimeoutMillis);

		RunResults newHistory = runnersFactory.build().run("pro.cyfronet.pl",
				"cat .bash_history | sha1sum", null, runTimeoutMillis);
		log.info("New history: {}", newHistory.getOutput());
		assertEquals(history.getOutput(), newHistory.getOutput());
	}

	@Test
	public void testLongOutput() throws CredentialException, KeyStoreException,
			CertificateException, GSSException, IOException,
			InterruptedException, Exception {
		long outputLength = 5120;
		RunResults result = runnersFactory.build().run("pro.cyfronet.pl",
				"cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w " + outputLength + " | head -n 1",
				null, runTimeoutMillis);
		assertEquals(outputLength, result.getOutput().length());
	}
}
