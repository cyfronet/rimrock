package pl.cyfronet.rimrock.db;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import pl.cyfronet.rimrock.domain.Job;
import pl.cyfronet.rimrock.repositories.JobRepository;

@SpringBootTest
public class DbBasicTest {
	@Autowired private JobRepository jobRepository;
	
	@BeforeEach
	public void setUp() {
		jobRepository.deleteAll();
	}
	
	@AfterEach
	public void tearDown() {
		jobRepository.deleteAll();
	}
	
	@Test
	public void testJobCrud() {
		Job job = new Job("jobId", "ACTIVE", "putput", "error", "user", "host", "tag");
		jobRepository.save(job);
		assertNotNull(job.getId());
		assertTrue(job.getId() > 0);
		
		long id = job.getId();
		Job found = jobRepository.findById(id).get();
		assertNotNull(found);
		assertEquals("jobId", found.getJobId());
		
		found.setJobId("anotherJobId");
		jobRepository.save(found);
		found = jobRepository.findById(id).get();
		assertEquals("anotherJobId", found.getJobId());
		
		jobRepository.deleteById(id);
		assertEquals(0, jobRepository.count());
	}
	
	@Test
	public void testFindByUserAndHosts() {
		Job j1 = userJob("1", "user1", "host1");
		Job j2 = userJob("2", "user1", "host2");
		jobRepository.save(j1);
		jobRepository.save(j2);
		jobRepository.save(userJob("3", "user1", "host3"));
		jobRepository.save(userJob("4", "user2", "host1"));
		
		List<Job> jobs = jobRepository.findByUsernameOnHosts("user1", Arrays.asList("host1", "host2"));
		
		assertEquals(2, jobs.size());		
		assertEquals("1", jobs.get(0).getJobId());
		assertEquals("2", jobs.get(1).getJobId());
	}
	
	@Test
	public void testGetHosts() {
		Job j1 = userJob("1", "user1", "host1");
		Job j2 = userJob("2", "user1", "host2");
		jobRepository.save(j1);
		jobRepository.save(j2);
		jobRepository.save(userJob("3", "user1", "host1"));
		jobRepository.save(userJob("4", "user2", "host2"));
		
		List<String> hosts = jobRepository.getHosts("user1");
		
		assertEquals(2, hosts.size());		
		assertThat(hosts).containsExactly("host1", "host2");
		
		hosts = jobRepository.getHosts("user2");
		assertThat(hosts).containsExactly("host2");
	}
	
	private Job userJob(String id, String username, String hostname) {
		return new Job(id, "ACTIVE", "", "", username, hostname, null);
	}
}