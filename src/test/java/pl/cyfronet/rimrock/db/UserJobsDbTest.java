package pl.cyfronet.rimrock.db;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import pl.cyfronet.rimrock.domain.Job;
import pl.cyfronet.rimrock.repositories.JobRepository;

@SpringBootTest
public class UserJobsDbTest {
	@Autowired
	private JobRepository jobRepository;
	
	@Test
	public void testNotTerminalJobQuery() {
		jobRepository.save(new Job("job-1", "RUNNING", "out", "error", "plglogin", "host1", "tag"));
		jobRepository.save(new Job("job-2", "ABORTED", "out", "error", "plglogin", "host1", "tag"));
		jobRepository.save(new Job("job-3", "FINISHED", "out", "error", "plglogin", "host1",
				"tag"));
		jobRepository.save(new Job("job-3", "FINISHED", "out", "error", "plglogin", "host1",
				"tag", "nodes", "cores", "walltime", "queuetime", "start", "end"));
		assertEquals(3, jobRepository.getNotTerminalJobIdsForUserLoginAndHost("plglogin",
				"host1").size());
	}
}