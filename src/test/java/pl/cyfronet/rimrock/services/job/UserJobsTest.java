package pl.cyfronet.rimrock.services.job;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.ietf.jgss.GSSException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.cyfronet.rimrock.domain.Job;
import pl.cyfronet.rimrock.gsi.ProxyHelper;
import pl.cyfronet.rimrock.repositories.JobRepository;
import pl.cyfronet.rimrock.services.RunnersFactory;
import pl.cyfronet.rimrock.runners.Gsissh;
import pl.cyfronet.rimrock.runners.RunException;
import pl.cyfronet.rimrock.runners.RunResults;

@SpringBootTest
public class UserJobsTest {
	@Mock private Gsissh runner;

	@Mock private RunnersFactory runnersFactory;
	@Mock private ProxyHelper proxyHelper;

	@Autowired private ObjectMapper mapper;
	@Autowired private JobRepository jobRepository;

	private UserJobs userJobs;
	private String proxy = "proxy";
	private String userLogin = "userLogin";

	@BeforeEach
	public void setup() throws Exception {
		MockitoAnnotations.openMocks(this);
		jobRepository.deleteAll();
		when(proxyHelper.getUserLogin(proxy)).thenReturn(userLogin);
		userJobs = new UserJobs(runner, jobRepository, proxyHelper, mapper);
	}
	
	@Test
	public void testSuccessJobSubmit() throws Exception {
		RunResults result = new RunResults();
		result.setExitCode(0);
		result.setTimeoutOccured(false);
		result.setOutput("{\"result\": \"OK\", \"job_id\": \"jobId\", \"standard_output\": \"stdout\", \"standard_error\": \"stderr\"}");

		when(runner.run(eq("pro.cyfronet.pl"), contains("script payload"), any(), anyInt()))
			.thenReturn(result);

		Job job = userJobs.submit("pro.cyfronet.pl", "/home/dir", "script payload", null);

		assertEquals("jobId", job.getJobId());
		assertEquals("QUEUED", job.getStatus());
		assertEquals(userLogin, job.getUserLogin());
		assertEquals("pro.cyfronet.pl", job.getHost());
		assertEquals("stdout", job.getStandardOutputLocation());
		assertEquals("stderr", job.getStandardErrorLocation());
	}

	@Test
	public void testRunFailureWhenSubmittingJob() throws Exception {
		when(
			runner.run(eq("pro.cyfronet.pl"),
					contains("script payload"),
					any(), anyInt())).thenThrow(new GSSException(1));

		try {
			userJobs.submit("pro.cyfronet.pl", "/home/dir", "script payload", null);
			fail();
		} catch (RunException e) {
			// ok should be thrown
		}
	}

	@Test
	public void testRunResponseCoruptedWhenSubmittingJob() throws Exception {
		RunResults result = new RunResults();
		result.setExitCode(0);
		result.setTimeoutOccured(false);
		result.setOutput("{\"corrupted\": true}");

		when(
			runner.run(eq("pro.cyfronet.pl"),
					contains("script payload"),
					any(), anyInt())).thenReturn(result);

		try {
			userJobs.submit("pro.cyfronet.pl", "/home/dir", "script payload", null);
			fail();
		} catch (RunException e) {
			// ok should be thrown
		}
	}

	@Test
	public void testUpdateJobStatus() throws Exception {
		createJob("1", userLogin, "pro.cyfronet.pl");
		createJob("2", userLogin, "pro.cyfronet.pl");
		createJob("3", userLogin, "login01.pro.cyfronet.pl");
		createJob("4", userLogin, "host3");
		createJob("5", "another_user", "pro.cyfronet.pl");

		RunResults proResult = new RunResults();
		proResult
				.setOutput("{\"statuses\": [{\"job_id\": \"1\", \"job_state\": \"RUNNING\"}]," +
						"\"history\": []," +
						"\"result\": \"OK\"}");

		RunResults login01Result = new RunResults();
		login01Result.setOutput("{\"statuses\": [{\"job_id\": \"3\", \"job_state\": \"ERROR\"}]," +
				"\"history\": []," +
				"\"result\": \"OK\"}");

		when(
				runner.run(
						eq("pro.cyfronet.pl"),
						contains("1, 2"),
						any(), anyInt())).thenReturn(proResult);
		when(
				runner.run(
						eq("login01.pro.cyfronet.pl"),
						contains("3"),
						any(), anyInt())).thenReturn(login01Result);

		userJobs.update(Arrays.asList("pro.cyfronet.pl", "login01.pro.cyfronet.pl"), null, null, false);

		assertEquals("RUNNING", jobStatus("1"));
		assertEquals("FINISHED", jobStatus("2"));
		assertEquals("ERROR", jobStatus("3"));
		assertEquals("QUEUED", jobStatus("4"));
		assertEquals("QUEUED", jobStatus("5"));
	}

	@Test
	public void testUpdateJobHistory() throws Exception {
		String jobId = "1";
		createJob(jobId, userLogin, "pro.cyfronet.pl", "FINISHED");

		RunResults proResult = new RunResults();
		proResult
				.setOutput("{\"statuses\": [], " +
						"\"history\": [{" +
						"\"job_id\": \"1\"," +
						"\"job_nodes\": \"1\"," +
						"\"job_cores\": \"12\"," +
						"\"job_walltime\": \"00:00:04\"," +
						"\"job_queuetime\": \"00:00:25\"," +
						"\"job_starttime\": \"2015-09-03 10:26:37\"," +
						"\"job_endtime\": \"2015-09-03 10:26:40\"" +
						"}], " +
						"\"result\": \"OK\"}");

		when(
				runner.run(
						eq("pro.cyfronet.pl"),
						contains("1"),
						any(), anyInt())).thenReturn(proResult);

		userJobs.update(Arrays.asList("pro.cyfronet.pl"), null, null, false);

		Job job = jobRepository.findOneByJobId(jobId);

		assertEquals("FINISHED", job.getStatus());
		assertEquals("1", job.getNodes());
		assertEquals("12", job.getCores());
		assertEquals("00:00:04", job.getWallTime());
		assertEquals("00:00:25", job.getQueueTime());
		assertEquals("2015-09-03 10:26:37", job.getStartTime());
		assertEquals("2015-09-03 10:26:40", job.getEndTime());
	}


	@Test
	public void testUpdateJobStatusesWhenNoHosts() throws Exception {
		List<Job> jobs = userJobs.update(Arrays.asList(), null, null, false);

		assertEquals(0, jobs.size());
	}

	@Test
	public void testDeleteRunningJob() throws Exception {
		createJob("to_delete", userLogin, "pro.cyfronet.pl");
		when(
				runner.run(
						eq("pro.cyfronet.pl"),
						contains("to_delete"),
						any(), anyInt())).thenReturn(new RunResults());

		userJobs.delete("to_delete", "pro.cyfronet.pl");

		assertNull(jobRepository.findOneByJobId("to_delete"));
	}

	@Test
	public void testDeleteFinishedJob() throws Exception {
		Job job = new Job("finished_to_delete", "FINISHED", "", "", userLogin, "pro.cyfronet.pl",
				null);
		jobRepository.save(job);

		userJobs.delete("finished_to_delete", "pro.cyfronet.pl");

		verify(runner, times(0))
				.run(eq("pro.cyfronet.pl"),
						contains("finished_to_delete"),
						any(), anyInt());
		assertNull(jobRepository.findOneByJobId("to_delete"));
	}

	@Test
	public void testGetUserJob() throws Exception {
		Job userJob = createJob("user_job", userLogin, "pro.cyfronet.pl");

		Job job = userJobs.get(userJob.getJobId(), userJob.getHost());

		assertEquals(userJob.getId(), job.getId());
	}

	public void testNotGetOtherUserJob() throws Exception {
		Job userJob = createJob("different_user_job", "different_user", "zeus.cyfronet.pl");

		Job job = userJobs.get(userJob.getJobId(), userJob.getHost());

		assertNull(job);
	}

	private Job createJob(String id, String username, String hostname) {
		return createJob(id, username, hostname, "QUEUED");
	}

	private Job createJob(String id, String username, String hostname, String status) {
		Job job = new Job(id, status, "", "", username, hostname, null);
		jobRepository.save(job);

		return job;
	}

	private String jobStatus(String jobId) {
		return jobRepository.findOneByJobId(jobId).getStatus();
	}
}
