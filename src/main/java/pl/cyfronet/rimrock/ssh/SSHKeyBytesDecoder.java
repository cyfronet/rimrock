package pl.cyfronet.rimrock.ssh;

import java.math.BigInteger;
import java.util.Arrays;

class SSHKeyBytesDecoder {
    private int pointer;
    private final byte[] body;

    private static final int UINT32_SIZE = 4;
    private static final int UINT64_SIZE = 8;

    SSHKeyBytesDecoder(byte[] body) {
        pointer = 0;
        this.body = body;
    }

    public BigInteger read_u32() {
        int endPointer = pointer + UINT32_SIZE;
        if (endPointer > body.length) {
            throw new IllegalArgumentException("Invalid ssh key");
        }
        byte[] val = Arrays.copyOfRange(body, pointer, endPointer);
        pointer = endPointer;
        return new BigInteger(val);
    }

    public BigInteger read_u64() {
        int endPointer = pointer + UINT64_SIZE;
        if (endPointer > body.length) {
            throw new IllegalArgumentException("Invalid ssh key");
        }
        byte[] val = Arrays.copyOfRange(body, pointer, endPointer);
        pointer = endPointer;
        return new BigInteger(val);
    }

    public byte[] read_str() {
        int length = read_u32().intValue();
        int endPointer = pointer + length;
        if (endPointer > body.length) {
            throw new IllegalArgumentException("Invalid ssh key");
        }
        byte[] val = Arrays.copyOfRange(body, pointer, endPointer);
        pointer = endPointer;
        return val;
    }

    public BigInteger read_mpint() {
        byte[] value = read_str();
        return new BigInteger(value);
    }

    public boolean isEmpty() {
        return pointer >= body.length;
    }
}

