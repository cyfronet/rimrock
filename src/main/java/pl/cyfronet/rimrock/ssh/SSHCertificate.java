package pl.cyfronet.rimrock.ssh;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.LinkedList;

public class SSHCertificate {
    private final String certificateText;
//    private final byte[] format;
    private final byte[] body;

    /*
     * Value encoding according to
     * https://github.com/openssh/openssh-portable/blob/master/PROTOCOL.certkeys
     * For more detailed descriptions of data type representations
     * used see RFC4249 Section 5.
     */
    private byte[] keyType;
    private byte[] nonce;
    private BigInteger exponent;
    private BigInteger publicModulus;
    private BigInteger uint62_serial;
    private BigInteger uint30_type;
    private byte[] keyId;
    private LinkedList<String> validPrincipals;
    private BigInteger uint62_validAfter;
    private BigInteger uint62_validBefore;
    private byte[] criticalOptions;
    private byte[] extensions;
    private byte[] reserved;
    private byte[] signatureKey;
    private byte[] signature;

    public SSHCertificate(String cert) {
        certificateText = cert;
        String[] parts = cert.trim().split(" ");
//        format = parts[-2].getBytes(StandardCharsets.UTF_8);
        body = Base64.getDecoder().decode(parts[1].getBytes(StandardCharsets.UTF_8));
        decode();
    }

    private void decode() {
        SSHKeyBytesDecoder bytesDecoder = new SSHKeyBytesDecoder(body);
        keyType = bytesDecoder.read_str();
        nonce = bytesDecoder.read_str();
        exponent = bytesDecoder.read_mpint();
        publicModulus = bytesDecoder.read_mpint();
        uint62_serial = bytesDecoder.read_u64();
        uint30_type = bytesDecoder.read_u32();
        keyId = bytesDecoder.read_str();

        byte[] principals = bytesDecoder.read_str();
        SSHKeyBytesDecoder principalsDecoder = new SSHKeyBytesDecoder(principals);
        validPrincipals = new LinkedList<>();
        while ( ! principalsDecoder.isEmpty()) {
            byte[] principal = principalsDecoder.read_str();
            validPrincipals.add(new String(principal));
        }

        uint62_validAfter = bytesDecoder.read_u64();
        uint62_validBefore = bytesDecoder.read_u64();
        criticalOptions = bytesDecoder.read_str();
        extensions = bytesDecoder.read_str();
        reserved = bytesDecoder.read_str();
        signatureKey = bytesDecoder.read_str();
        signature = bytesDecoder.read_str();
    }

    public String getUserLogin() {
        return getValidPrincipals().getFirst();
    }

    public String getCertificateText() {
        return certificateText;
    }

    public byte[] getKeyType() {
        return keyType;
    }

    public byte[] getNonce() {
        return nonce;
    }

    public BigInteger getExponent() {
        return exponent;
    }

    public BigInteger getPublicModulus() {
        return publicModulus;
    }

    public BigInteger getSerial() {
        return uint62_serial;
    }

    public BigInteger getType() {
        return uint30_type;
    }

    public byte[] getKeyId() {
        return keyId;
    }

    public LinkedList<String> getValidPrincipals() {
        return validPrincipals;
    }

    public BigInteger getValidAfter() {
        return uint62_validAfter;
    }

    public BigInteger getValidBefore() {
        return uint62_validBefore;
    }

    public byte[] getCriticalOptions() {
        return criticalOptions;
    }

    public byte[] getExtensions() {
        return extensions;
    }

    public byte[] getReserved() {
        return reserved;
    }

    public byte[] getSignatureKey() {
        return signatureKey;
    }

    public byte[] getSignature() {
        return signature;
    }
}