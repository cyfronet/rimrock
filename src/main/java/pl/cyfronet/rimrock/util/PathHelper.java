package pl.cyfronet.rimrock.util;

import java.util.regex.Pattern;

import pl.cyfronet.rimrock.errors.UnknownHostException;

public class PathHelper {
	private static final Pattern PRO_PATTERN = Pattern.compile("^(login\\d+\\.)?pro(metheus)?\\.cyf(ronet|-kr\\.edu)\\.pl$");
	private static final Pattern ARES_PATTERN = Pattern.compile("^(login\\d+\\.)?ares\\.cyf(ronet|-kr\\.edu)\\.pl$");
	private static final Pattern ATHENA_PATTERN = Pattern.compile("^(login\\d+\\.)?athena\\.cyf(ronet|-kr\\.edu)\\.pl$");

	
	public void validateHost(String host) throws UnknownHostException {
		if (!PRO_PATTERN.matcher(host).matches() && 
			!ARES_PATTERN.matcher(host).matches() && 
			!ATHENA_PATTERN.matcher(host).matches()) {
			throw new UnknownHostException(host);
		}
	}
}
 