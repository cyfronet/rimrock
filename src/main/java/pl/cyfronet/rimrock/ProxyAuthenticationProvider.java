package pl.cyfronet.rimrock;

import org.globus.gsi.CredentialException;
import org.jboss.logging.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import pl.cyfronet.rimrock.connectors.Connector;
import pl.cyfronet.rimrock.errors.BadCredentialsException;
import pl.cyfronet.rimrock.gsi.ProxyHelper;

public class ProxyAuthenticationProvider implements AuthenticationProvider {
	private static final Logger log = LoggerFactory.getLogger(ProxyAuthenticationProvider.class);
	
	private ProxyHelper proxyHelper;
	
	public ProxyAuthenticationProvider(ProxyHelper proxyHelper) {
		this.proxyHelper = proxyHelper;
	}
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		Connector connector = (Connector) authentication.getCredentials();
		log.debug("Authenticating user with login {}", authentication.getName());
		log.trace("Authenticating user credentials {}", connector.toCredentialsString());
		
		try {
			connector.verify();
		} catch (CredentialException e) {
			log.warn("Bad credentials sent", e);
			
			throw new BadCredentialsException("Bad credentials provided: " + e.getMessage(), e);
		}


		PreAuthenticatedAuthenticationToken result = new PreAuthenticatedAuthenticationToken(authentication.getName(), authentication.getCredentials());
		
		
		MDC.put("userLogin", authentication.getName());
		
		return result;
	}

	@Override
	public boolean supports(Class<?> c) {
		return c.isAssignableFrom(PreAuthenticatedAuthenticationToken.class);
	}
}