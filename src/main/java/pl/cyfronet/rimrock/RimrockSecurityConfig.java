package pl.cyfronet.rimrock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import pl.cyfronet.rimrock.errors.CustomErrorAttributes;
import pl.cyfronet.rimrock.filters.ProxyHeaderPreAuthenticationProcessingFilter;
import pl.cyfronet.rimrock.gsi.ProxyHelper;

@Configuration
@EnableWebSecurity
public class RimrockSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${unsecure.api.resources}")	private String unsecureApiResources;
	
	@Autowired ProxyAuthenticationProvider proxyAuthenticationProvider;
	
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(proxyAuthenticationProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.
			authorizeRequests().
				antMatchers(unsecureApiResources.split(",")).
					permitAll().
				antMatchers("/api/**").
					fullyAuthenticated().
				anyRequest().
					permitAll().
					and().
			addFilter(proxyHeaderfilter(authenticationManager())).
			sessionManagement().
				sessionCreationPolicy(SessionCreationPolicy.STATELESS).
				and().
			csrf().
				disable();
	}

	protected ProxyHeaderPreAuthenticationProcessingFilter proxyHeaderfilter(AuthenticationManager authenticationManager) {
		return new ProxyHeaderPreAuthenticationProcessingFilter(authenticationManager);
	}
}