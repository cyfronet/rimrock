package pl.cyfronet.rimrock.runners;

import com.google.common.io.ByteStreams;
import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.cyfronet.rimrock.services.Pool;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Runner {
    private static final Logger log = LoggerFactory.getLogger(Runner.class);

    private static final HashSet<PosixFilePermission> PERMISSIONS = Stream
            .of(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE)
            .collect(Collectors.toCollection(HashSet::new));

    protected final String userLogin;
    protected final int runTimeoutMillis;
    private final Pool pool;

    public Runner(String userLogin, Pool pool, int runTimeoutMillis) {
        this.userLogin = userLogin;
        this.pool = pool;
        this.runTimeoutMillis = runTimeoutMillis;
    }

    public abstract RunResults run(String host, String command, String workingDirectory, int timeoutMillis)
            throws CredentialException, InterruptedException, GSSException, IOException;

    protected RunResults doRun(String[] initCommand, String[] env, String command, int timeoutMillis) throws InterruptedException, IOException {
        try {
            pool.checkPool(userLogin);
            log.trace("Running (command): {}", command);
            log.trace("Running (init command): {}", Arrays.toString(initCommand));
            log.trace("Running (env): {}", Arrays.toString(env));

            Process process = Runtime.getRuntime().exec(initCommand, env);

            OutputStream os = process.getOutputStream();
            PrintWriter writer = new PrintWriter(os);

            writer.write(command);
            writer.close();
            timeoutMillis = timeoutMillis > 0 ? timeoutMillis : runTimeoutMillis;

            if (process.waitFor(timeoutMillis, TimeUnit.MILLISECONDS)) {
                return readResult(process);
            } else {
                log.info("Destroying process because timeout occurs");
                process.destroy();
                RunResults results = new RunResults();
                results.setTimeoutOccured(true);
                return results;
            }
        } finally {
            pool.freePool(userLogin);
        }
    }

    protected String getDestination(String host) {
        return userLogin + "@" + host;
    }

    private RunResults readResult(Process process) throws IOException {
        ByteArrayOutputStream standardOutput = new ByteArrayOutputStream();
        ByteStreams.copy(process.getInputStream(), standardOutput);

        ByteArrayOutputStream standardError = new ByteArrayOutputStream();
        ByteStreams.copy(process.getErrorStream(), standardError);

        RunResults results = new RunResults();

        results.setOutput(standardOutput.toString().trim());
        results.setError(standardError.toString().trim());
        results.setExitCode(process.exitValue());

        return results;
    }

    protected void protectAndWriteTo(File file, String payload) throws IOException {
        Files.setPosixFilePermissions(file.toPath(), PERMISSIONS);
        writeTo(file, payload);
    }

    private void writeTo(File file, String payload) throws IOException {
        FileWriter fw = new FileWriter(file);
        fw.write(payload);
        fw.close();
    }

    protected String getInitCommand(String workingDirectory) {
        if (workingDirectory != null && !workingDirectory.isBlank()) {
            return "cd " + workingDirectory + "; bash -s";
        }
        return "bash -s";
    }

    public String getUserLogin() {
        return userLogin;
    }
}
