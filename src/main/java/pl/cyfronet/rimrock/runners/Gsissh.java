package pl.cyfronet.rimrock.runners;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.cyfronet.rimrock.services.Pool;

import java.io.File;
import java.io.IOException;

public class Gsissh extends Runner {
    private static final Logger log = LoggerFactory.getLogger(Gsissh.class);

    private final String proxyValue;

    int runTimeoutMillis;

    public Gsissh(String userLogin, String proxyValue, Pool pool, int runTimeoutMillis) {
        super(userLogin, pool, runTimeoutMillis);
        this.proxyValue = proxyValue;
    }

    /**
     * Runs the given command on the host. Authentication uses the given proxy. If
     * given timeout (provided in millis) is greater than 0 it is used, otherwise a
     * default value is used.
     */
    @Override
    public RunResults run(String host, String command, String workingDirectory, int timeoutMillis)
            throws CredentialException, InterruptedException, GSSException, IOException {
        File proxyFile = File.createTempFile("proxy_" + userLogin + "_", ".crt");
        try {
            protectAndWriteTo(proxyFile, proxyValue);

            String[] initCommand = {
                    "/usr/bin/gsissh",
                    "-o", "LogLevel=error", "-o", "StrictHostKeyChecking=no", //"-o", "PreferredAuthentications=gssapi-with-mic",
                    host, getInitCommand(workingDirectory)
            };
            String[] env = {"X509_USER_PROXY=" + proxyFile.getAbsolutePath()};

            return doRun(initCommand, env, command, timeoutMillis);
        } finally {
            proxyFile.delete();
        }
    }
}