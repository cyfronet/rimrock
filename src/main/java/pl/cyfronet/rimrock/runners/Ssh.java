package pl.cyfronet.rimrock.runners;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.cyfronet.rimrock.services.Pool;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ssh extends Runner {

    private static final Logger log = LoggerFactory.getLogger(Runner.class);
    private final String key;

    private final String keyCert;

    public Ssh(String userLogin, String key, String keyCert, Pool pool, int runTimeoutMillis) {
        super(userLogin, pool, runTimeoutMillis);
        this.key = key;
        this.keyCert = keyCert;
    }
    @Override
    public RunResults run(String host, String command, String workingDirectory, int timeoutMillis) throws CredentialException, InterruptedException, GSSException, IOException {
        File keyFile = File.createTempFile("ssh_" + userLogin, "key");
        File keyCertFile = new File(keyFile.getAbsolutePath() + "-cert.pub");
        keyCertFile.createNewFile();
        try {
            protectAndWriteTo(keyFile, key);
            protectAndWriteTo(keyCertFile, keyCert);

            String[] initCommand = {
                    "/usr/bin/ssh",
                    "-o", "LogLevel=error", "-o", "StrictHostKeyChecking=no", "-o", "IdentitiesOnly=yes",
                    "-i", keyFile.getAbsolutePath(),
                    getDestination(host), getInitCommand(workingDirectory)
            };

            String[] env = {};

            return doRun(initCommand, env, command, timeoutMillis);
        } finally {
            keyFile.delete();
            keyCertFile.delete();
        }
    }
}
