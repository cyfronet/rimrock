package pl.cyfronet.rimrock;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import pl.cyfronet.rimrock.errors.CustomErrorAttributes;
import pl.cyfronet.rimrock.gsi.ProxyHelper;

@SpringBootApplication
@EnableConfigurationProperties
public class RimrockApplication implements WebMvcConfigurer {
	private static final Logger log = LoggerFactory.getLogger(RimrockApplication.class);
	
	@Autowired
	private RequestLoggingInterceptor loggingInterceptor;

	public static void main(String[] args) {
		SpringApplication.run(RimrockApplication.class, args);
		log.info("rimrock application successfully started");
	}

	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(loggingInterceptor)
        		.addPathPatterns("/api/**");
    }
	
	@Bean
    LocaleResolver localeResolver() {
		CookieLocaleResolver clr = new CookieLocaleResolver();
        clr.setDefaultLocale(Locale.US);
        clr.setCookieName("rimrock-lang");
        
        return clr;
    }
 
    @Bean
    LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        
        return lci;
    }

	@Bean
	protected ProxyAuthenticationProvider proxyAuthenticationProvider(ProxyHelper proxyHelper) {
		return new ProxyAuthenticationProvider(proxyHelper);
	}
	
	@Bean
	protected ErrorAttributes errorAttributes() {
		return new CustomErrorAttributes();
	}
}
