package pl.cyfronet.rimrock.filters;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import pl.cyfronet.rimrock.connectors.Connector;
import pl.cyfronet.rimrock.connectors.GridProxy;
import pl.cyfronet.rimrock.connectors.InvalidConnector;
import pl.cyfronet.rimrock.connectors.Ssh;
import pl.cyfronet.rimrock.gsi.ProxyHelper;

import java.util.Enumeration;

public class ProxyHeaderPreAuthenticationProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {
	private static final Logger log = LoggerFactory.getLogger(ProxyHeaderPreAuthenticationProcessingFilter.class);
	
	private ProxyHelper proxyHelper = new ProxyHelper();
	
	public ProxyHeaderPreAuthenticationProcessingFilter(AuthenticationManager authenticationManager) {
		setAuthenticationManager(authenticationManager);
	}

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		return getConnector(request).getUserLogin();
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		return getConnector(request);
	}

	public Connector getConnector(HttpServletRequest request) {
		String proxyValue = getEncodedHeaderValue(request, "PROXY");
		if (proxyValue != null) {
			return new GridProxy(proxyValue);
		} else {
			String sshKey = getEncodedHeaderValue(request, "SSH_KEY");
			String sshKeyCert = getEncodedHeaderValue(request, "SSH_KEY_CERT");

			if (sshKey != null && sshKeyCert != null) {
				return new Ssh(sshKey, sshKeyCert);
			}
		}

		return new InvalidConnector();
	}
	
	private String getEncodedHeaderValue(HttpServletRequest request, String key) {
		String proxyValue = request.getHeader(key);

		if(proxyValue != null) {
			try {
				return proxyHelper.decodeProxy(proxyValue);
			} catch(Throwable e) {
				log.warn("Could not properly process " + key + " value", e);
				//ignoring - null will be returned
			}
		}

		return null;
	}
}