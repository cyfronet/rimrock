package pl.cyfronet.rimrock.errors;

import static org.springframework.security.web.WebAttributes.AUTHENTICATION_EXCEPTION;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

import java.util.Map;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.context.request.WebRequest;

public class CustomErrorAttributes extends DefaultErrorAttributes {
	@Override
	public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
		Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, options);
		addAuthenticationError(webRequest, errorAttributes);
		
		return errorAttributes;
	}
	
	private void addAuthenticationError(WebRequest webRequest, Map<String, Object> errorAttributes) {
		Throwable error = (Throwable) webRequest.getAttribute(AUTHENTICATION_EXCEPTION, SCOPE_REQUEST);
		
		if(error != null) {
			errorAttributes.put("error_message", error.getMessage());
		}
	}
}