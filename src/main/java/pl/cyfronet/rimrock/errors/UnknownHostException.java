package pl.cyfronet.rimrock.errors;

public class UnknownHostException extends Exception {
	
	private String host;
	
	public UnknownHostException(String host) {
		this.host = host;
	}

	public String getHost() {
		return host;
	}

	@Override
	public String getMessage() {
		return "Unknown host (" + host + ")";
	}
}
