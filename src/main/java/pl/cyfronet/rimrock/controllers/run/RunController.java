package pl.cyfronet.rimrock.controllers.run;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;

import javax.validation.Valid;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.cyfronet.rimrock.controllers.RunResponse;
import pl.cyfronet.rimrock.controllers.RunResponse.Status;
import pl.cyfronet.rimrock.controllers.ValidationException;
import pl.cyfronet.rimrock.errors.UnknownHostException;
import pl.cyfronet.rimrock.services.RunnersFactory;
import pl.cyfronet.rimrock.runners.RunException;
import pl.cyfronet.rimrock.runners.RunResults;
import pl.cyfronet.rimrock.util.PathHelper;

@RestController
public class RunController {
	private static final Logger log = LoggerFactory.getLogger(RunController.class);

	@Value("${run.timeout.millis}") private int runTimeoutMillis;

	private RunnersFactory runnersFactory;

	@Autowired
	public RunController(RunnersFactory runnersFactory) {
		this.runnersFactory = runnersFactory;
	}

	@PostMapping(value = "/api/process", 
			consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<RunResponse> run(@Valid @RequestBody RunRequest runRequest, BindingResult errors)
			throws CredentialException, InterruptedException, GSSException, IOException, UnknownHostException {
		log.debug("Processing run request {}", runRequest);

		if(errors.hasErrors()) {
			throw new ValidationException(errors);
		}
		new PathHelper().validateHost(runRequest.getHost());

		RunResults results = runnersFactory.build().run(runRequest.getHost(),
				runRequest.getCommand(), runRequest.getWorkingDirectory(), -1);

		if(results.isTimeoutOccured()) {
			throw new RunException(
					"timeout occurred; maximum allowed execution time for this operation is "
			+ runTimeoutMillis + " ms", results);
		}

		return new ResponseEntity<>(
				new RunResponse(Status.OK, results.getExitCode(), results.getOutput(),
						results.getError(), null), OK);
	}
}