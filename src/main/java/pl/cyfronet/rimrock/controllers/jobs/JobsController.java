package pl.cyfronet.rimrock.controllers.jobs;

import static java.util.Arrays.asList;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.cyfronet.rimrock.controllers.ErrorResponse;
import pl.cyfronet.rimrock.controllers.ValidationException;
import pl.cyfronet.rimrock.domain.Job;
import pl.cyfronet.rimrock.errors.UnknownHostException;
import pl.cyfronet.rimrock.runners.RunException;
import pl.cyfronet.rimrock.services.job.JobNotFoundException;
import pl.cyfronet.rimrock.services.job.UserJobs;
import pl.cyfronet.rimrock.services.job.UserJobsFactory;
import pl.cyfronet.rimrock.util.PathHelper;

@RestController
public class JobsController {
	private static final Logger log = LoggerFactory.getLogger(JobsController.class);

	private UserJobsFactory userJobsFactory;

	@Value("${plgridData.url}")	private String plgDataUrl;
	
	@Autowired
	public JobsController(UserJobsFactory userJobsFactory) {
		this.userJobsFactory = userJobsFactory;
	}

	@PostMapping(value = "/api/jobs", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<JobInfo> submit(@Valid @RequestBody SubmitRequest submitRequest, BindingResult errors)
			throws CredentialException, GSSException, RunException, KeyStoreException,
			CertificateException, IOException, UnknownHostException {

		if (errors.hasErrors()) {
			throw new ValidationException(errors);
		}
		new PathHelper().validateHost(submitRequest.getHost());

		UserJobs manager = userJobsFactory.build();

		Job job = manager.submit(submitRequest.getHost(), submitRequest.getWorkingDirectory(),
				submitRequest.getScript(), submitRequest.getTag());

		return new ResponseEntity<>(new JobInfo(job), CREATED);
	}

	@GetMapping(value = "/api/jobs/{jobId:.+?(?=.)}.{host:.+}", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<JobInfo> jobInfo(@PathVariable("jobId") String jobId, @PathVariable("host") String host)
			throws JobNotFoundException, CredentialException, GSSException, IOException, InterruptedException,
			KeyStoreException, CertificateException, RunException, UnknownHostException {

		log.debug("Processing status request for job with id {} started on {}", jobId, host);

		UserJobs manager = userJobsFactory.build();
		Job job = manager.get(jobId, host);

		if (!Job.END_STATES.contains(job.getStatus())
				|| job.getCores() == null) {
			manager.update(Arrays.asList(job.getHost()), null, Collections.singletonMap(host, asList(jobId)), false);
			job = manager.get(jobId, host);
		}

		return new ResponseEntity<>(new JobInfo(job), OK);
	}
	
	@GetMapping(value = "/api/jobs", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<JobInfo>> globalStatus(
			@RequestParam(value = "tag", required = false) String tag,
			@RequestParam(value = "format", required = false) String format,
			@RequestParam(value = "job_id", required = false) List<String> jobIds)
			throws CredentialException, GSSException, 
			IOException, InterruptedException, KeyStoreException, CertificateException,
			RunException {
		UserJobs manager = userJobsFactory.build();
		List<Job> jobs = manager.update(null, tag, buildJobIdsMap(jobIds), "short".equalsIgnoreCase(format));
		List<JobInfo> infos = jobs.stream().
				map(job -> new JobInfo(job)).
				collect(Collectors.toList());

		return new ResponseEntity<>(infos, OK);
	}

	private Map<String, List<String>> buildJobIdsMap(List<String> jobIds) {
		Pattern jobIdPattern = Pattern.compile("\\A(?<jid>[^.]+).(?<host>.+)\\Z");
		if (jobIds != null) {
			return jobIds.stream()
					.map(jobId -> jobIdPattern.matcher(jobId))
					.filter(matcher -> matcher.find())
					.collect(Collectors.groupingBy(m -> m.group("host"),
							 Collectors.mapping(m -> m.group("jid"), Collectors.toList())));
		} else {
			return null;
		}
	}

	@DeleteMapping(value = "/api/jobs/{jobId:.+?(?=.)}.{host:.+}", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> deleteJob(
			@PathVariable("jobId") String jobId, @PathVariable("host") String host)
			throws CredentialException, GSSException, JobNotFoundException,
			KeyStoreException, CertificateException, IOException, RunException {
		UserJobs manager = userJobsFactory.build();
		manager.delete(jobId, host);

		return new ResponseEntity<>(NO_CONTENT);
	}

	@PutMapping(value = "/api/jobs/{jobId:.+?(?=.)}.{host:.+}",
			consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> processJobAction(
			@Valid @RequestBody JobActionRequest actionRequest, BindingResult errors,
			@PathVariable("jobId") String jobId, @PathVariable("host") String host)
			throws CredentialException, GSSException,JobNotFoundException,
			KeyStoreException, CertificateException, IOException, RunException {
		if(errors.hasErrors()) {
			throw new ValidationException(errors);
		}

		if(actionRequest.getAction().equalsIgnoreCase("abort")) {
			UserJobs manager = userJobsFactory.build();
			manager.abort(jobId, host);
		}

		return new ResponseEntity<>(NO_CONTENT);
	}

	@ExceptionHandler(JobNotFoundException.class)
	private ResponseEntity<ErrorResponse> handleJobNotFoundError(JobNotFoundException e) {
		return new ResponseEntity<>(new ErrorResponse(e.getMessage()), NOT_FOUND);
	}
}