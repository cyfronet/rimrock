package pl.cyfronet.rimrock.controllers.jobs;

public class JobActionRequest {
	private String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}