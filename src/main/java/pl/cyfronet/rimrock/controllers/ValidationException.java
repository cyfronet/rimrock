package pl.cyfronet.rimrock.controllers;

import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;

public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(BindingResult errors) {
		this(convertErrors(errors));
	}

	private static String convertErrors(BindingResult errors) {
		return errors.getFieldErrors().stream().map(f -> {
			return f.getField() + ": " + String.join(", ", f.getCodes());
		}).collect(Collectors.joining("; "));
	}
}
