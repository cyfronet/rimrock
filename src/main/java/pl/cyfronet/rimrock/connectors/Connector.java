package pl.cyfronet.rimrock.connectors;

import pl.cyfronet.rimrock.runners.Runner;
import pl.cyfronet.rimrock.services.Pool;

public interface Connector {
    void verify() throws org.globus.gsi.CredentialException;

    Runner build(Pool pool, int runTimeoutMillis);

    String getUserLogin();

    String toCredentialsString();
}
