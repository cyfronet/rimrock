package pl.cyfronet.rimrock.connectors;

import org.globus.gsi.CredentialException;
import org.globus.gsi.OpenSSLKey;
import org.globus.gsi.X509Credential;
import org.globus.gsi.bc.BouncyCastleOpenSSLKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.cyfronet.rimrock.gsi.ProxyHelper;
import pl.cyfronet.rimrock.runners.Runner;
import pl.cyfronet.rimrock.services.Pool;
import pl.cyfronet.rimrock.runners.Gsissh;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GridProxy implements Connector {
    public static final String PROXY_PARTS_PATTERN = "(.+)-----BEGIN PRIVATE KEY-----(.*)-----END PRIVATE KEY-----(.+)";

    public static final String LOGIN_FROM_DN_PATTERN = ".+,CN=(plg.*?)(,.*$|$)";

    private static final Logger log = LoggerFactory.getLogger(GridProxy.class);

    private final String proxyValue;

    private ProxyHelper helper = new ProxyHelper();

    private Function<String, String> getUserLoginFromDNField = dn -> {
        Pattern pattern = Pattern.compile(LOGIN_FROM_DN_PATTERN);
        Matcher matcher = pattern.matcher(dn);

        if(matcher.matches()) {
            String login = matcher.group(1);
            log.debug("DN contained login: {} {}", dn, login);

            return login;
        } else {
            return null;
        }
    };

    public GridProxy(String proxyValue) {
        this.proxyValue = proxyValue;
    }

    public void verify() throws CredentialException {
        helper.verify(proxyValue);
    }

    @Override
    public Runner build(Pool pool, int runTimeoutMillis) {
        return new Gsissh(getUserLogin(), proxyValue, pool, runTimeoutMillis);
    }

    public String getUserLogin() {
        String dn = null;
        try {
            dn = getX509Credential(proxyValue).getIssuer();
            List<Function<String, String>> converters = Arrays.asList(getUserLoginFromDNField);

            for (Function<String, String> converter : converters) {
                String login = converter.apply(dn);
                if (login != null) {
                    log.debug("DN was converted to login: {} {}", dn, login);
                    return login;
                }
            }
        } catch (CredentialException e) {
            throw new IllegalArgumentException("Unable to parse proxy DN");
        }

        //throw new IllegalArgumentException("Could not extract user name from the supplied user proxy with DN " + dn);
        return dn;
    }

    @Override
    public String toCredentialsString() {
        return proxyValue;
    }

    private X509Credential getX509Credential(String proxyValue) throws CredentialException {
        try {
            //Until https://github.com/jglobus/JGlobus/issues/146 is fixed and released the following fix
            //needs to be used. It converts the PKCS#8 format generated by voms-proxy-init to a format
            //recognized by X509Credential.
            if(proxyValue.contains("BEGIN PRIVATE KEY")) {
                Pattern p = Pattern.compile(PROXY_PARTS_PATTERN, Pattern.DOTALL);
                Matcher m = p.matcher(proxyValue);

                if(m.matches()) {
                    String firstCert = m.group(1).trim();
                    String key = m.group(2).trim();
                    String secondCert = m.group(3).trim();
                    key = key.replaceAll("\n", "");

                    byte[] decoded = Base64.getDecoder().decode(key);
                    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);
                    KeyFactory kf = KeyFactory.getInstance("RSA");
                    PrivateKey privateKey = kf.generatePrivate(spec);
                    OpenSSLKey openSSLKey = new BouncyCastleOpenSSLKey(privateKey);
                    StringWriter writer = new StringWriter();
                    openSSLKey.writeTo(writer);
                    proxyValue = firstCert + "\n" + writer.toString() + "\n" + secondCert;
                }
            }

            return new X509Credential(new ByteArrayInputStream(proxyValue.getBytes()));
        } catch(Exception e) {
            throw new CredentialException("Could not load user proxy certificate", e);
        }
    }
}
