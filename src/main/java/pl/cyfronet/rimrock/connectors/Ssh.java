package pl.cyfronet.rimrock.connectors;

import org.globus.gsi.CredentialException;
import pl.cyfronet.rimrock.runners.Runner;
import pl.cyfronet.rimrock.ssh.SSHCertificate;
import pl.cyfronet.rimrock.services.Pool;

public class Ssh implements Connector {
    private final String keyCert;
    private final String key;

    public Ssh(String key, String keyCert) {
        this.key = key;
        this.keyCert = keyCert;
    }

    @Override
    public void verify() throws CredentialException {
//        throw new CredentialException("Not implemented yet");
    }

    @Override
    public Runner build(Pool pool, int runTimeoutMillis) {
        return new pl.cyfronet.rimrock.runners.Ssh(getUserLogin(), key, keyCert, pool, runTimeoutMillis);
    }

    @Override
    public String getUserLogin() {
        return new SSHCertificate(keyCert).getUserLogin();
    }

    @Override
    public String toCredentialsString() {
        return key + "\n\n" + keyCert;
    }
}