package pl.cyfronet.rimrock.connectors;

import org.globus.gsi.CredentialException;
import pl.cyfronet.rimrock.runners.Runner;
import pl.cyfronet.rimrock.services.Pool;

public class InvalidConnector implements Connector {
    @Override
    public void verify() throws CredentialException {
        throw new CredentialException("No valid credentials given");
    }

    @Override
    public Runner build(Pool pool, int runTimeoutMillis) {
        return null;
    }

    @Override
    public String getUserLogin() {
        return null;
    }

    @Override
    public String toCredentialsString() {
        return null;
    }
}
