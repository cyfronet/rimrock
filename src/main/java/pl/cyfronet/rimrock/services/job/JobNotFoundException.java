package pl.cyfronet.rimrock.services.job;

import pl.cyfronet.rimrock.errors.ResourceNotFoundException;

public class JobNotFoundException extends ResourceNotFoundException {

	private static final long serialVersionUID = 1L;

	public JobNotFoundException(String jobId, String host) {
		super(String.format("Job with %s id started on %s does not exist", jobId, host));
	}
}
