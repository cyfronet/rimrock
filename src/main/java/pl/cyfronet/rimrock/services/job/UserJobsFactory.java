package pl.cyfronet.rimrock.services.job;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.cyfronet.rimrock.gsi.ProxyHelper;
import pl.cyfronet.rimrock.repositories.JobRepository;
import pl.cyfronet.rimrock.services.RunnersFactory;

@Service
public class UserJobsFactory {

	@Autowired
	private RunnersFactory runnersFactory;
	
	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	private ProxyHelper proxyHelper;
	
	@Autowired
	private ObjectMapper mapper;

	public UserJobs build() throws CredentialException, GSSException, KeyStoreException, CertificateException, IOException {
		return new UserJobs(runnersFactory.build(), jobRepository, proxyHelper, mapper);
	}
}
