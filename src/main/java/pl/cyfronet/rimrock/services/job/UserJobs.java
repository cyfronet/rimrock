package pl.cyfronet.rimrock.services.job;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.globus.gsi.CredentialException;
import org.ietf.jgss.GSSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.cyfronet.rimrock.runners.Runner;
import pl.cyfronet.rimrock.domain.Job;
import pl.cyfronet.rimrock.errors.UnknownHostException;
import pl.cyfronet.rimrock.gsi.ProxyHelper;
import pl.cyfronet.rimrock.repositories.JobRepository;
import pl.cyfronet.rimrock.runners.RunException;
import pl.cyfronet.rimrock.runners.RunResults;
import pl.cyfronet.rimrock.util.ResourceReader;

public class UserJobs {
	private static final Logger log = LoggerFactory.getLogger(UserJobs.class);

	private int timeout = 20000;
	private String userLogin;
	private Runner runner;
	private JobRepository jobRepository;
	private ObjectMapper mapper;

	public UserJobs(Runner runner, JobRepository jobRepository, ProxyHelper proxyHelper,
					ObjectMapper mapper)
			throws CredentialException, GSSException, KeyStoreException, CertificateException, IOException {

		this.runner = runner;
		this.userLogin = runner.getUserLogin();
		this.jobRepository = jobRepository;
		this.mapper = mapper;
	}

	/**
	 * Submit new job into queues system.
	 *
	 * @param host             Host where job will be submitted
	 * @param workingDirectory Job working directory.
	 * @param script           Job script payload.
	 * @return Information about job status, std oud and std err file paths.
	 * @throws UnknownHostException
	 */
	public Job submit(String host, String workingDirectory, String script, String tag)
			throws CredentialException, RunException, KeyStoreException, CertificateException, UnknownHostException {

		String runScript = ResourceReader
				.asString(new ClassPathResource("scripts/start"))
				.replaceFirst("<<%%script%%>>", Matcher.quoteReplacement(script));

		RunResults result = run(host, runScript, timeout);
		processRunExceptions(result);

		SubmitResult submitResult = readResult(result.getOutput(), SubmitResult.class);

		if ("OK".equals(submitResult.getResult())) {
			String jobStatus = "QUEUED";
			log.info("Local job {} sbumitted and saved with status {}", submitResult.getJobId(), jobStatus);
			return jobRepository
					.save(new Job(submitResult.getJobId(), jobStatus, submitResult.getStandardOutputLocation(),
							submitResult.getStandardErrorLocation(), userLogin, host, tag));
		} else {
			throw new RunException(submitResult.getErrorMessage(), result);
		}
	}

	/**
	 * Update job statuses started on selected hosts.
	 *
	 * @param hosts Limit hosts to the supplied ones. If null is given all hosts for
	 *              a given user will be used based on job history.
	 * @param tag
	 * @return Updated jobs.
	 */
	public List<Job> update(List<String> hosts, String tag, Map<String, List<String>> overrideJobIds, boolean shortFormat)
			throws CredentialException, RunException, KeyStoreException, CertificateException {

		if (hosts == null) {
			hosts = jobRepository.getHosts(userLogin);
		}
		
		if (overrideJobIds != null && overrideJobIds.size() > 0) {
			hosts.retainAll(overrideJobIds.keySet());
		}

		if (hosts.size() == 0) {
			return Arrays.asList();
		}

		List<Status> statuses = new ArrayList<>();
		List<History> histories = new ArrayList<>();

		for (String host : hosts) {
			List<String> jobIds = jobRepository.getNotTerminalJobIdsForUserLoginAndHost(userLogin, host);

			if (overrideJobIds != null && overrideJobIds.getOrDefault(host, Arrays.asList()).size() > 0) {
				jobIds.retainAll(overrideJobIds.get(host));
			}

			if (jobIds.size() > 0) {
				StatusResult statusResult = getStatusResult(host, jobIds, shortFormat);

				if (statusResult.getErrorMessage() != null) {
					throw new RunException(statusResult.getErrorMessage());
				}

				statuses.addAll(statusResult.getStatuses());
				histories.addAll(statusResult.getHistory());
			}
		}

		List<Job> jobs = null;

		if (tag != null) {
			jobs = jobRepository.findByUsernameAndTagOnHosts(userLogin, tag, hosts);
		} else {
			jobs = jobRepository.findByUsernameOnHosts(userLogin, hosts);
		}

		if (overrideJobIds != null && overrideJobIds.size() > 0) {
			jobs = jobs.stream()
					.filter(job -> overrideJobIds.getOrDefault(job.getHost(), new ArrayList<String>()).contains(job.getJobId()))
					.collect(Collectors.toList());
		}

		// toMap uses a BinaryOperator as the third parameter to deal with status
		// duplicates
		Map<String, Status> mappedStatusJobIds = statuses.stream()
				.collect(Collectors.toMap(Status::getJobId, Function.<Status>identity(), (a, b) -> a));

		// toMap uses a BinaryOperator as the third parameter to deal with history
		// duplicates
		Map<String, History> mappedHistoryJobIds = histories.stream()
				.collect(Collectors.toMap(History::getJobId, Function.<History>identity(), (a, b) -> a));

		for (Job job : jobs) {
			String status = mappedStatusJobIds.get(job.getJobId()) != null
					&& mappedStatusJobIds.get(job.getJobId()).getStatus() != null
							? mappedStatusJobIds.get(job.getJobId()).getStatus()
							: "FINISHED";
			History history = mappedHistoryJobIds.get(job.getJobId());

			// changing job status only if new state is different than the old one
			if (!job.getStatus().equals(status)) {
				if (Job.END_STATES.contains(job.getStatus())) {
					log.warn("Local job {} with a terminal state ({}) attempt to change to {} " + "prevented",
							job.getJobId(), job.getStatus(), status);
				} else {
					log.info("Local job {} changed status from {} to {}", job.getJobId(), job.getStatus(), status);
					job.setStatus(status);
					jobRepository.save(job);
				}
			}

			if (Job.END_STATES.contains(job.getStatus()) && job.getCores() == null
					&& history != null) {
				job.setNodes(history.getJobNodes());
				job.setCores(history.getJobCores());
				job.setWallTime(history.getJobWalltime());
				job.setQueueTime(history.getJobQueuetime());
				job.setStartTime(history.getJobStarttime());
				job.setEndTime(history.getJobEndtime());
				jobRepository.save(job);
			}

		}

		return jobs;
	}

	/**
	 * Delete job. If job is in state different then "FINISHED", than it is also
	 * deleted from the infrastructure.
	 */
	public void delete(String jobId, String host)
			throws JobNotFoundException, CredentialException, RunException, KeyStoreException, CertificateException {

		Job job = abortJob(jobId, host);
		log.info("Local job {} deleted", job.getJobId());
		jobRepository.delete(job);
	}

	public void abort(String jobId, String host)
			throws CredentialException, RunException, JobNotFoundException, KeyStoreException, CertificateException {

		Job job = abortJob(jobId, host);
		job.setStatus("ABORTED");
		log.info("Local job {} aborted", job.getJobId());
		jobRepository.save(job);
	}

	public Job get(String jobId, String host) throws JobNotFoundException {
		Job job = jobRepository.findOneByJobIdAndHostAndUserLogin(jobId, host, userLogin);

		if (job == null) {
			throw new JobNotFoundException(jobId, host);
		}
		
		return job;
	}

	private Job abortJob(String jobId, String host)
			throws JobNotFoundException, CredentialException, RunException, KeyStoreException, CertificateException {

		Job job = get(jobId, host);

		if (!"FINISHED".equals(job.getStatus())) {
			String script = ResourceReader.asString(new ClassPathResource("scripts/stop"))
					.replaceFirst("<<%%ids%%>>", jobId);

			log.debug("Abort job script:", script);

			RunResults result = run(host, script, timeout);
			processRunExceptions(result);
			log.info("Local job {} aborted on the computing infrastructure as it was not " + "completed yet", jobId);
		}

		return job;
	}

	private <T> T readResult(String output, Class<T> klass) {
		try {
			return mapper.readValue(output, klass);
		} catch (Exception e) {
			throw new RunException(e.getMessage());
		}
	}

	private StatusResult getStatusResult(String host, List<String> jobIds, boolean shortFormat)
			throws CredentialException, RunException, KeyStoreException, CertificateException {
		
		String scriptPath = shortFormat ? "scripts/status-short" : "scripts/status";
		
		String script = ResourceReader
				.asString(new ClassPathResource(scriptPath))
				.replaceFirst("<<%%ids%%>>", jobIds.stream().collect(Collectors.joining(",")));
		
		log.debug("Get status result script:", script);
		
		RunResults result = run(host, script, timeout);

		if (result.isTimeoutOccured() || result.getExitCode() != 0) {
			StatusResult statusResult = new StatusResult();
			statusResult.setResult("ERROR");
			statusResult.setErrorMessage(result.getError());

			if ((statusResult.getErrorMessage() == null || statusResult.getErrorMessage().isEmpty())
					&& result.isTimeoutOccured()) {
				statusResult.setErrorMessage("Timeout occurred");
			}

			return statusResult;
		} else {
			return readResult(result.getOutput(), StatusResult.class);
		}
	}

	private RunResults run(String host, String command, int timeout)
			throws CredentialException, RunException, KeyStoreException, CertificateException {

		try {
			RunResults runResults = runner.run(host, command, null, timeout);
			log.debug("Run results for command [{}] are the following: {}", command, runResults);

			return runResults;
		} catch (GSSException | IOException | InterruptedException e) {
			throw new RunException(e.getMessage(), e);
		}
	}

	private void processRunExceptions(RunResults result) {
		if (result.isTimeoutOccured()) {
			throw new RunException("Unable to submit new job because of timeout", result);
		}

		if (result.getExitCode() != 0) {
			throw new RunException("Submission of new job failed", result);
		}
	}
}
