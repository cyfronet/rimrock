package pl.cyfronet.rimrock.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class Pool {
    private static final Logger log = LoggerFactory.getLogger(Pool.class);

    @Value("${gsissh.pool.size}")
    int poolSize;

    private final Map<String, AtomicInteger> logins = new HashMap<>();

    public void checkPool(String userLogin) throws InterruptedException {
        synchronized (logins) {
            if (!logins.containsKey(userLogin)) {
                logins.put(userLogin, new AtomicInteger(0));
            }

            while (logins.get(userLogin).get() >= poolSize) {
                log.debug("Thread {} awaits for gsissh execution", Thread.currentThread().getId());
                logins.wait();
            }

            log.debug("Thread {} granted gsissh execution", Thread.currentThread().getId());
            logins.get(userLogin).incrementAndGet();
        }
    }

    public void freePool(String userLogin) {
        synchronized (logins) {
            log.debug("Thread {} frees gsissh execution", Thread.currentThread().getId());

            int size = logins.get(userLogin).decrementAndGet();

            if (size == 0) {
                logins.remove(userLogin);
            }

            logins.notify();
        }
    }
}
