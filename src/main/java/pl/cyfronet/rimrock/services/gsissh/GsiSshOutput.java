package pl.cyfronet.rimrock.services.gsissh;

import java.io.InputStream;

public class GsiSshOutput {
    private final InputStream stdOut;
    private final InputStream stdErr;

    public GsiSshOutput(InputStream out, InputStream err) {
        this.stdOut = out;
        this.stdErr = err;
    }

    public InputStream getStdOut() {
        return stdOut;
    }

    public InputStream getStdErr() {
        return stdErr;
    }
}
