package pl.cyfronet.rimrock.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.cyfronet.rimrock.connectors.Connector;
import pl.cyfronet.rimrock.runners.Runner;

@Service
public class RunnersFactory {
    @Value("${run.timeout.millis}")
	private int runTimeoutMillis;

    @Autowired
	private Pool pool;

    public Runner build() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Connector connector = (Connector)authentication.getCredentials();

        return connector.build(pool, runTimeoutMillis);
    }
}
